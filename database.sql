--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1
-- Dumped by pg_dump version 14.1

-- Started on 2022-12-16 14:03:35

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3327 (class 1262 OID 50849)
-- Name: tpsecurityUserDetails; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE "tpsecurityUserDetails" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'French_France.1252';


ALTER DATABASE "tpsecurityUserDetails" OWNER TO postgres;

\connect "tpsecurityUserDetails"

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 3328 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 209 (class 1259 OID 50999)
-- Name: client; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client (
    email character varying(255) NOT NULL,
    name character varying(255),
    password character varying(255)
);


ALTER TABLE public.client OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 51006)
-- Name: client_roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_roles (
    clients_email character varying(255) NOT NULL,
    roles_id bigint NOT NULL
);


ALTER TABLE public.client_roles OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 51010)
-- Name: role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.role (
    id bigint NOT NULL,
    name character varying(255)
);


ALTER TABLE public.role OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 51009)
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_id_seq OWNER TO postgres;

--
-- TOC entry 3329 (class 0 OID 0)
-- Dependencies: 211
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.role_id_seq OWNED BY public.role.id;


--
-- TOC entry 3172 (class 2604 OID 51013)
-- Name: role id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role ALTER COLUMN id SET DEFAULT nextval('public.role_id_seq'::regclass);


--
-- TOC entry 3318 (class 0 OID 50999)
-- Dependencies: 209
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.client VALUES ('anais@anais.fr', 'anais', '$2a$10$NeiWKZinvXLOf6D6KWPcG.tGZCxKmilLB9miLLtIveITIrwpBZzIe');
INSERT INTO public.client VALUES ('pierre@pierre.fr', 'pierre', '$2a$10$AJB50k1tuo4.vMo51Z1JMO2OXyAAnnngnShhn/sug.nqsBZxOxIZ.');


--
-- TOC entry 3319 (class 0 OID 51006)
-- Dependencies: 210
-- Data for Name: client_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.client_roles VALUES ('anais@anais.fr', 1);
INSERT INTO public.client_roles VALUES ('pierre@pierre.fr', 2);


--
-- TOC entry 3321 (class 0 OID 51010)
-- Dependencies: 212
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.role VALUES (1, 'USER');
INSERT INTO public.role VALUES (2, 'ADMIN');


--
-- TOC entry 3330 (class 0 OID 0)
-- Dependencies: 211
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.role_id_seq', 1, false);


--
-- TOC entry 3174 (class 2606 OID 51005)
-- Name: client client_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT client_pkey PRIMARY KEY (email);


--
-- TOC entry 3176 (class 2606 OID 51015)
-- Name: role role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- TOC entry 3178 (class 2606 OID 51021)
-- Name: client_roles fkqelnxllu3ofrp88gnn2o3scjd; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_roles
    ADD CONSTRAINT fkqelnxllu3ofrp88gnn2o3scjd FOREIGN KEY (clients_email) REFERENCES public.client(email);


--
-- TOC entry 3177 (class 2606 OID 51016)
-- Name: client_roles fksgkujj8649e59exwr5rlu1p0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_roles
    ADD CONSTRAINT fksgkujj8649e59exwr5rlu1p0 FOREIGN KEY (roles_id) REFERENCES public.role(id);


-- Completed on 2022-12-16 14:03:35

--
-- PostgreSQL database dump complete
--


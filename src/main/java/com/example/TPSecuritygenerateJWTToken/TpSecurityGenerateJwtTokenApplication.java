package com.example.TPSecuritygenerateJWTToken;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpSecurityGenerateJwtTokenApplication {

	public static void main(String[] args) {
		SpringApplication.run(TpSecurityGenerateJwtTokenApplication.class, args);
	}

}

package com.example.TPSecuritygenerateJWTToken.application;


import com.example.TPSecuritygenerateJWTToken.domaine.Client;

public interface IClientService {

    Client findByEmail(String email);
}
